package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

//Identity : definición de estructura de datos
type Identity struct {
	Name string `json:"Name"`
	Age  int    `json:"Age"`
	Sex  string `json:"Sex"`
}

var person map[int]Identity

func main() {

	var person = getPersons()

	router := gin.Default()
	router.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "Ok. Welcome to index page, if you want to see a list of people add to the address bar /welcome")
	})
	router.GET("/welcome", func(c *gin.Context) {
		c.JSON(http.StatusOK, person)
	})
	router.Run(":8081")
}

func getPersons() map[int]Identity {
	return map[int]Identity{
		1: {"José", 33, "Male"},
		2: {"María", 30, "Female"},
		3: {"Manuel", 30, "Male"},
		4: {"Belen", 30, "Female"},
		5: {"Alberto", 16, "Male"},
		6: {"Ana", 3, "Female"},
	}
}
