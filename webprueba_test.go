package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPrueba(t *testing.T) {
	persons := getPersons()
	assert.Len(t, persons, 6)
}
