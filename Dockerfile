FROM golang:1.8
WORKDIR /go/src/webprueba
COPY . .
RUN go get -d -v ./...
RUN go install -v ./...
CMD ["webprueba"]
EXPOSE 8081
